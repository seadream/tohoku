(function($){

/* ----------------------------------------------------------------------------------
　heightLine用プラグイン
---------------------------------------------------------------------------------- */

$.fn.heightLineClass = function( options ){
	if(!$(this).length){
		return;
	}
	// デフォルト値を構築
	var defaults = {
		'number' : 2,//1行につき何カラムあるか
		'class':false,//対象のクラス名。下のtagとこれどちらかを必ず指定する。
		'tag':false,//対象をタグにする時に書く。上のclassとは両立不可。
		'hlClass':'E',//付与クラスのheightLine-と番号の間の文字列
		'maxwidth':false,//特定の横幅以上ではクラスを無くしたい場合幅を書く
		'minwidth':false//特定の横幅以下ではクラスを無くしたい場合幅を書く（上と複合設定可）
	};


	// 引数とデフォルト値を比較し、変数[setting]へ格納する
	var setting = $.extend( defaults, options );


	if(!setting['class'] && !setting.tag){
		return;

	}
	if(setting['class'] && setting.tag){
		return;

	}


	var parrent = $(this)
	if(setting['class']){
		var target = '.' + setting['class'];
	}else{
		var target = setting.tag;
	}

	var re = new RegExp("\\bheightLine-"+setting.hlClass+"\\d+","g");
	// heightline用classを付けてheightlineを起動する
	function ClassOn(){
		var num = 0;
		parrent.each(function(){
			$(this).find(target).each(function(i){
				if(i%setting.number < 1 && i != 0){
					num++;
				}
				 $(this).addClass('heightLine-'+setting.hlClass+ num);

			});
			num++;
		});
		heightLine(parrent.find('*').get());
	}
	function ClassOff(){
		parrent.each(function(){
			$(this).find(target).each(function(){
				$(this).attr('style','');
				$(this).removeClass (function (index, css) {
					return (css.match (re) || []).join(' ');
				});

			});
			heightLine(parrent.find('*').get());
		});
	}


	if(setting.maxwidth || setting.minwidth){//幅設定されてた時の記述
		var timer = false;
		var flag = false;

		if(window.innerWidth <= setting.maxwidth && window.innerWidth > setting.minwidth){
			ClassOn();
			flag = true;
		}
		if(setting.maxwidth && setting.minwidth){


			$(window).resize(function() {//ウィンドウ幅が変更されたら
				if (timer !== false) {
					clearTimeout(timer);
				}
				timer = setTimeout(function() {
					if(window.innerWidth <= setting.maxwidth && window.innerWidth > setting.minwidth){
						if(!flag){
							ClassOn();
							flag = true;
						}
					}else{
						if(flag){
							ClassOff();
							flag = false;
						}
					}
				}, 200);
			});
		}else if(setting.maxwidth){
			if(window.innerWidth <= setting.maxwidth){
				ClassOn();
				flag = true;
			}
			$(window).resize(function() {//ウィンドウ幅が変更されたら
				if (timer !== false) {
					clearTimeout(timer);
				}
				timer = setTimeout(function() {
					if(window.innerWidth <= setting.maxwidth){

						if(!flag){
							ClassOn();
							flag = true;
						}
					}else{
						if(flag){
							ClassOff();
							flag = false;
						}
					}
				}, 200);
			});
		}else if(setting.minwidth){
			if(window.innerWidth > setting.minwidth){
				ClassOn();
				flag = true;
			}
			$(window).resize(function() {//ウィンドウ幅が変更されたら
				if (timer !== false) {
					clearTimeout(timer);
				}
				timer = setTimeout(function() {
					if(window.innerWidth > setting.minwidth){
						if(!flag){
							ClassOn();
							flag = true;
						}
					}else{
						if(flag){
							ClassOff();
							flag = false;
						}
					}
				}, 200);
			});
		}
	}else{
		ClassOn();
	}
	return( this );
}

})(jQuery);

