(function($){

	// カスタムスクロールバー Data Table(通常時)
	$(window).on('load resize', function(){
		var w = 'innerWidth' in window ? window.innerWidth : document.body.clientWidth;
		if (w <= 765){
			$(".Table_Data").mCustomScrollbar({
				horizontalScroll : true
			});
		}else{
			$(".Table_Data").mCustomScrollbar("destroy");
		}
	});

})(jQuery);