/* ----------------------------------------------------------------------------------
デバイス判定
---------------------------------------------------------------------------------- */
UA = window.navigator.userAgent.toLowerCase();
IS_TB = UA.match(/ipad|android(?!.*mobile)/) ? true : false;
IS_ANDOROID = UA.match(/android/) ? true : false;

/* Media Query PC判定 */
function isPC() {
	var w = 'innerWidth' in window ? window.innerWidth : document.body.clientWidth;
	return (767 < w) ? true : false;
}

(function($){

/* Portrait | landscape 判定 */
function setView(){
	var orientation = window.orientation;
	if(orientation === 0){
		$("body").addClass("portrait").removeClass("landscape");
	}else{
		$("body").addClass("landscape").removeClass("portrait");
	}
}

$(function() {
	/* Tablet Add Class */
	if (IS_TB) { $('body').addClass('tablet'); }

	/* SetView */
	if (IS_ANDOROID) {
		window.onresize = setView;
	} else {
		window.onorientationchange = setView;
	}
});

/* ----------------------------------------------------------------------------------
スムーズスクロール
---------------------------------------------------------------------------------- */
var scroll_func = function () {
	$('html,body').animate({ scrollTop: $($(this).attr("href")).offset().top }, 'slow','swing');
	return false;
}
var scroll_func2 = function () {
	var diff = isPC() ? 0 : 43;
	$('html,body').animate({ scrollTop: $($(this).attr("href")).offset().top - diff }, 'slow','swing');
	return false;
}

$(function(){
	$('#PageTop a').click(scroll_func);
	$('[href^=#]').not('[href$=#], #PageTop a').click(scroll_func2);
});

/* ----------------------------------------------------------------------------------
　ロールオーバー
---------------------------------------------------------------------------------- */
$.fn.rollover2 = function() {
	return this.each(function() {
		var target = this;
		$.each($(this).find('img[src*=_off]'), function(){
			var img = this;
			$(img).addClass('rollover2');
			if($(img).prev().is('[src*=_on]')){}else{

				var overimg = $(document.createElement('img')).attr('src', img.src.replace(/_off/, '_on'));
				$(overimg).css({ opacity:0, position:'absolute' })
				$(img).before(overimg);

				if($(overimg).parents('button').length){
					$(overimg).parents('button').hover(function(){
						var hoverimg = $(this).find('img')[0];
						$(hoverimg).stop().animate({ opacity:1 }, 300);
					},function(){
						var hoverimg = $(this).find('img')[0];
						$(hoverimg).stop().animate({ opacity:0 }, 300);
					});
				}else{
					$(overimg).hover(function(){
						var hoverimg = this;
						$(hoverimg).stop().animate({ opacity:1 }, 300);
					},function(){
						var hoverimg = this;
						$(hoverimg).stop().animate({ opacity:0 }, 300);
					});
				}

			}
		});
	});
};
$(function(){
	$('a img[src*=_on]').hover(function(){
		$(this).css({ opacity:1 });
	});
});
$(function(){ $('body').rollover2(); });

/* ----------------------------------------------------------------------------------
Load Scroll
---------------------------------------------------------------------------------- */
$(window).on('load', function() {
	var targetPagesClass = '.PageAccess, .PageAcademics';
	if ($(targetPagesClass).length) {
		var param = location.search.split('?')[1];
		if (param) {
			var $target = $('#' + param);
			if ($target.length) {
				var diff = isPC() ? 0 : 43;
				$('html,body').animate({ scrollTop: $target.offset().top - diff }, 'slow','swing');
			}
		}
	}
});

/* ----------------------------------------------------------------------------------
SPメニュー(Search & Menu)
---------------------------------------------------------------------------------- */
$(function() {
	/* init */
	var $body = $('body');
	var $Gnav = $('#GlobalNav');
	$body.append('<div class="Overlay" />');
	$Gnav.append('<div class="GnavSpacer" />')

	/* Search */
	var $SpSearchBtn = $('#SpSearchBtn a');
	var $SearchBox = $('#SearchBox');
	$SpSearchBtn.on('click', function(e){
		e.preventDefault();
		$SearchBox.toggleClass('active');
		if($SearchBox.is('.active')){
			$SearchBox.find('input').focus();
		}
		$body.toggleClass('OPEN_Search');
		return false;
	});

	/* Menu() */
	var $Overlay = $('.Overlay');
	var $GnavSpacer = $('.GnavSpacer');
	var $SpMenuBtn = $('#SpMenuBtn a');
	var $LanguageSelect = $('.LanguageSelect a.NavAcd');
	var $HeadNav = $('.HeadNav a.NavAcd');
	var $GnavParent = $('#GlobalNav .NavParent li');
	var $GnavParent = $('#GlobalNav .NavParent');
	var $GnavParentAco = $GnavParent.find('> li > a.NavAco');
	var $MegaNavAco = $('.MegaNav a.NavAco i');
	/* Menu(LanguageSelect) */
	$LanguageSelect.on('click', function() {
		$HeadNav.closest('.ParentLi').removeClass('active');
		$(this).closest('.LanguageSelect').toggleClass('active');
		return false;
	});
	/* Menu(HeadNav) */
	$HeadNav.on('click', function() {
		$(this).closest('.ParentLi').siblings().removeClass('active');
		$LanguageSelect.closest('.LanguageSelect').removeClass('active');
		$(this).closest('.ParentLi').toggleClass('active');
		return false;
	});
	/* Menu(Gnav) */
	$SpMenuBtn.on('click', function() {
		if($body.is('.OPEN_Search')) { $SpSearchBtn.trigger('click'); }
		if ($body.is('.OPEN')) {
			closeAll();
		} else {
			$body.toggleClass('OPEN');
			$SearchBox.removeClass('active');
			/**/setGnavHeight();
		}
		return false;
	});
	/* Menu(GnavParentAco) */
	$GnavParentAco.on('click', function() {
		if (!isPC() || IS_TB) {
			$(this).closest('li').toggleClass('active');
			$(this).next().stop().slideToggle(300, function() {
				setGnavHeight();
			});
		}
		closeHeadNav();
		return false;
	});
	/* Menu(MegaNavAco) */
	if (!isPC()) {
		$MegaNavAco.on('click.meganav', onClickMegaNavAco);
		$GnavParent.addClass('setOnClickParentNav');
	}
	function onClickMegaNavAco() {
		$(this).closest('li').toggleClass('active');
		$(this).parent().next().stop().slideToggle(300, function() {
			setGnavHeight();
		});
		closeHeadNav();
		return false;
	}

	/* Overlay(Close) */
	$Overlay.add($GnavSpacer).on('click', function() {
		closeAll();
	});

	$('.GlobalNavInner').on('touchstart', function() {
		closeHeadNav();
	});

	function setGnavHeight() {
		var winH = window.innerHeight;
		var HeadH = $('#Head').height();
		var GnavInnerH = $Gnav.find('.GlobalNavInner').height();
		if (winH - (HeadH + GnavInnerH) < 0) {
			$Gnav.height(winH - HeadH);
			$Gnav.addClass('scroll');
		} else {
			$Gnav.attr('style', '');
			$Gnav.removeClass('scroll');
		}
	}

	function closeAll() {
		$body.removeClass('OPEN');
		if($body.is('.OPEN_Search')) { $SpSearchBtn.trigger('click'); }
		$HeadNav.closest('.ParentLi').removeClass('active');
		$LanguageSelect.closest('.LanguageSelect').removeClass('active');
		//$MegaNavAco.closest('li').removeClass('active');
		//$GnavParentAco.closest('li').removeClass('active');
		//$GnavParentAco.next().stop().delay(300).slideUp();
		$Gnav.attr('style', '');
		$Gnav.removeClass('scroll');
	}

	function closeHeadNav() {
		$HeadNav.closest('.ParentLi').removeClass('active');
		$LanguageSelect.closest('.LanguageSelect').removeClass('active');
	}

	function setCurrentSpGNav(bodyClass, bodySubClass, path) {
		var bodyClassStr = 'body.' + bodyClass + '.' + bodySubClass;
		if ($(bodyClassStr).length) {
			var $target = $('#GlobalNav .MegaNav > ul > li > a[href*="' + path + '"]');
			console.log($target);
			$target.trigger('click');
		}
	}

	var timer = false;
	var FirstFlag = true;
	$(window).on('load resize', function() {
		if (timer !== false) { clearTimeout(timer); }
		timer = setTimeout(function() {
			if (isPC() && !IS_TB) {
				closeAll();
				$MegaNavAco.off('click.meganav');
				$GnavParent.removeClass('setOnClickParentNav');
			} else if ($body.is('.OPEN')) {
				setGnavHeight();
			} else if (!$GnavParent.hasClass('setOnClickParentNav')){
				$MegaNavAco.on('click.meganav', onClickMegaNavAco);
				$GnavParent.addClass('setOnClickParentNav');
			}
			if (!isPC() && FirstFlag) {
				setCurrentSpGNav('PageAcademics', 'PageUnderGraduate', '/academics/undergraduate');
				setCurrentSpGNav('PageAcademics', 'PageGraduate', '/academics/graduate');
				setCurrentSpGNav('PageResearch', 'PageTokai', '/research/tokai');
				setCurrentSpGNav('PageCampusLife', 'PageAcademicCalendar', '/campus/academic_calendar');
				setCurrentSpGNav('PageCampusLife', 'PageCampuses', '/campus/campuses');
				setCurrentSpGNav('PageCampusLife', 'PageStudentSupport', '/campus/student_support');
				setCurrentSpGNav('PageCampusLife', 'PageStudentLife', '/campus/student_life');
				FirstFlag = false;
			}
		}, 200);
	});
});


/* ----------------------------------------------------------------------------------
ページTOPボタン
---------------------------------------------------------------------------------- */
$(function() {
	var isFirst = true;
	var isPageHome = $('body.PageHome').length ? true : false;
	var pageTopIsActive = false;
	var $pageTop = $('#PageTop');
	function pageTopOn () { $pageTop.addClass('active').removeClass('activeTop'); pageTopIsActive = true; }
	function pageTopOff () { $pageTop.removeClass('active').removeClass('activeTop'); pageTopIsActive = false; }

	$(window).on('scroll load', function() {
		var diff = isPC() ? isPageHome ? 59 : 19 : 50;
		if (0 < $(window).scrollTop()) {
			if ( ($('html').height() - ($('#Foot').height() - diff)) <= ($(window).scrollTop() + window.innerHeight) ) {
				if (pageTopIsActive || isFirst) {
					pageTopOff();
				}
			} else if (!pageTopIsActive) {
				pageTopOn();
			}
		} else if (pageTopIsActive) {
			$pageTop.addClass('activeTop');
			pageTopIsActive = false;
		}
		removeClassActiveTop();
		isFirst = false;
	});

	var timer = false;
	$(window).on('load resize', function() {
		if (timer !== false) { clearTimeout(timer); }
		timer = setTimeout(function() {
			removeClassActiveTop();
		}, 200);
	});

	function removeClassActiveTop() {
		var $FootWrap = $('#FootWrap');
		var winH = window.innerHeight;
		var HeadH = $('#Head').height();
		var LowerMainvisH = $('#LowerMainvis').length ? $('#LowerMainvis').height() : 0;
		var MainH = $('#Main').height();
		var FootWrapH = $FootWrap.height();
		var diffB = isPC() ? 19 : 50;
		if (diffB <= (winH - (HeadH + MainH))) {
			$pageTop.removeClass('active activeTop');
		}
	}

});

/* ----------------------------------------------------------------------------------
Footer Fixed
---------------------------------------------------------------------------------- */
$(function() {
	var $FootWrap = $('#FootWrap');
	var timer = false;
	$(window).on('load resize', function() {
		if (timer !== false) { clearTimeout(timer); }
		timer = setTimeout(function() {
			if (isPC() && !IS_TB) { $FootWrap.removeClass('fixed'); return false; }
			setFooterFixed();
		}, 200);
	});
	function setFooterFixed() {
		var winH = window.innerHeight;
		var HeadH = $('#Head').height();
		var LowerMainvisH = $('#LowerMainvis').length ? $('#LowerMainvis').height() : 0;
		var MainH = $('#Main').height();
		var FootWrapH = $FootWrap.height();
		if (0 < (winH - (HeadH + MainH + LowerMainvisH + FootWrapH))) {
			$FootWrap.addClass('fixed');
		} else {
			$FootWrap.removeClass('fixed');
		}
	}
});

/* ----------------------------------------------------------------------------------
Accodion Lside
---------------------------------------------------------------------------------- */
$(function(){
	$('.LsideAccordion i').on('click',function(){
		$(this).closest('a').toggleClass('active');
		$(this).closest('a').next().slideToggle('fast');
		return false;
	});

	function setActiveLsideNavParent (bodyClass, targetName) {
		var targetNameAry = targetName.split(' ');
		$.each(targetNameAry, function(key, val) {
			var bodyClassStr = 'body.' + bodyClass + '.Page' + val;
			if ($(bodyClassStr).length) {
				var targetLsideClass = '.LsideNav .Aco' + val;
				$(targetLsideClass).addClass('active');
				$(targetLsideClass).next().slideToggle('fast');
			}
		});
	}
	setActiveLsideNavParent('PageAboutTokai', 'Greetings Founder History Today Mission Govemance Facts International Overseas Guidelines');
	setActiveLsideNavParent('PageAdmissions', 'JapaneseLanguageCouse Exchange Short-term');
	setActiveLsideNavParent('PageAcademics', 'UnderGraduate Graduate JapaneseLanguageEducation Minors Other Degrees');
	setActiveLsideNavParent('PageResearch', 'Institutes Tokai Seeds Visits');
	setActiveLsideNavParent('PageCampusLife', 'StudentSupport StudentLife Voices AcademicCalendar Campuses Outreach Alumni');
});

/* ----------------------------------------------------------------------------------
Lside CurrentLink
---------------------------------------------------------------------------------- */
$(function() {
	$('#Lside .LsideList a').each(function(){
		location_h = location.href.replace(location.hash, '').replace('index.html','');
		if(this.href.replace('index.html','')==location_h){
			$(this).addClass('Current');
		}
	});
});

/* ----------------------------------------------------------------------------------
Youtube
---------------------------------------------------------------------------------- */
$(function(){
	var timer;
	var modalWindow = '<div id="MovieModal"></div>';
	$('.PopUpItem').on('click', function(e) {
		e.preventDefault();
		var mvHref   = $(this).attr('href');
		var mvWidth  = 850;
		var mvHeight = 478;
		if(window.innerWidth<=768){
			var mvWidth  = 850;
			var mvHeight = 478;
		}
		clearTimeout(timer);
		timer = setTimeout(function(){
			$(modalWindow).hide().appendTo('body').fadeIn(150, function () {
				$(this).append('<div class="FrameWrap"><iframe src="' + mvHref + '?autoplay=1&amp;rel=0" width="' + mvWidth + '" height="' + mvHeight + '" frameborder="0"></iframe></div>');
			});
		}, 100);
	});

	$('body').on('click','#MovieModal', function() {
		$(this).fadeOut(150, function() {
			$(this).remove();
		});
	});
});

})(jQuery);
