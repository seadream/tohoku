var ua = window.navigator.userAgent;
if (ua.match(/iPad|Android(?!.*mobile)/i)) {
     document.write('<meta name="viewport" content="width=768">');
} else {
     document.write('<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">');
}
