(function($){
$(function() {

	/* ImgColumns */
	try {
		$('.ImgColumns.Column-2').not('pre .ImgColumns').heightLineClass({
			'number' : 2,
			'class':false,
			'tag':'figcaption',
			'hlClass':'Col2-'
		});

		$('.ImgColumns.Column-3').not('pre .ImgColumns').heightLineClass({
			'number' : 3,
			'class':false,
			'tag':'figcaption',
			'hlClass':'Col3-'
		});

		$('.ImgColumns.Column-4').not('pre .ImgColumns').heightLineClass({
			'number' : 4,
			'class':false,
			'tag':'figcaption',
			'hlClass':'Col4-'
		});
	} catch (e) {}

});
})(jQuery);
