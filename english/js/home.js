(function($){
	// flexslider
	$(window).load(function() {
		$('.FlexSlider').flexslider({
			animation: "fade",
			selector : ".Slides > li",
			slideshow: true, // スライドショーをautoにするか
			initDelay: 0, // オープンからスライドショーが始まるまでの待機時間
			slideshowSpeed: 5000, // 切り替わるまでの待機時間
			animationSpeed: 1000, // アニメーションスピード
			pauseOnAction: true, // ページャー等クリック後、スライドショーを続けるか止めるか
			directionNav: false
		});
	});
})(jQuery);
