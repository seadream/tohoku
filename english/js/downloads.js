(function($){
	// heightLineClass
	$(function() {
		$('.DlBoxLists').heightLineClass({
			'number' : 6,//1行につき何カラムあるか
			'class':"DlListItem",//対象のクラス名。下のtagとこれどちらかを必ず指定する。
			'hlClass':'facility',//付与クラスのheightLine-と番号の間の文字列
			'minwidth':769//特定の横幅以下ではクラスを無くしたい場合幅を書く（上と複合設定可）
		});
		$('.DlBoxLists').heightLineClass({
			'number' : 4,
			'class':"DlListItem",
			'hlClass':'facility',
			'maxwidth':768,
			'minwidth':668
		});
		$('.DlBoxLists').heightLineClass({
			'number' : 2,
			'class':"DlListItem",
			'hlClass':'facility',
			'maxwidth':667
		});
	});


})(jQuery);
