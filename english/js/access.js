(function($){
    $(function() {
        google.maps.event.addDomListener(window, 'load', init);
        function init() {
            var mapOptions = {
                zoom: 15,
                center: new google.maps.LatLng(35.36578076172648, 139.2741322517395),
            };
            var mapElement = document.getElementById('Shonan_map');
            var map = new google.maps.Map(mapElement, mapOptions);
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(35.36578076172648, 139.2741322517395),
    			icon: "/english/common/images/tokai-designator.png",
                map: map,
                title: ''
            });
            var mapOptions = {
                zoom: 15,
                center: new google.maps.LatLng(35.66451, 139.683747),
            };
            var mapElement = document.getElementById('Yoyogi_map');
            var map = new google.maps.Map(mapElement, mapOptions);
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(35.66451, 139.683747),
                icon: "/english/common/images/tokai-designator.png",
                map: map,
                title: ''
            });
            var mapOptions = {
                zoom: 15,
                center: new google.maps.LatLng(35.637667, 139.734238),
            };
            var mapElement = document.getElementById('Takanawa_map');
            var map = new google.maps.Map(mapElement, mapOptions);
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(35.637667, 139.734238),
                icon: "/english/common/images/tokai-designator.png",
                map: map,
                title: ''
            });
            var mapOptions = {
                zoom: 15,
                center: new google.maps.LatLng(35.406921, 139.315178),
            };
            var mapElement = document.getElementById('Isehara_map');
            var map = new google.maps.Map(mapElement, mapOptions);
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(35.406921, 139.315178),
                icon: "/english/common/images/tokai-designator.png",
                map: map,
                title: ''
            });
            var mapOptions = {
                zoom: 15,
                center: new google.maps.LatLng(34.988716, 138.515173),
            };
            var mapElement = document.getElementById('Shimizu_map');
            var map = new google.maps.Map(mapElement, mapOptions);
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(34.988716, 138.515173),
                icon: "/english/common/images/tokai-designator.png",
                map: map,
                title: ''
            });
            var mapOptions = {
                zoom: 15,
                center: new google.maps.LatLng(42.991291, 141.317886),
            };
            var mapElement = document.getElementById('Sapporo_map');
            var map = new google.maps.Map(mapElement, mapOptions);
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(42.991291, 141.317886),
                icon: "/english/common/images/tokai-designator.png",
                map: map,
                title: ''
            });
            var mapOptions = {
                zoom: 15,
                center: new google.maps.LatLng(32.812311, 130.745067),
            };
            var mapElement = document.getElementById('Kumamoto_map');
            var map = new google.maps.Map(mapElement, mapOptions);
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(32.812311, 130.745067),
                icon: "/english/common/images/tokai-designator.png",
                map: map,
                title: ''
            });
            var mapOptions = {
                zoom: 15,
                center: new google.maps.LatLng(32.891474, 130.995287),
            };
            var mapElement = document.getElementById('Aso_map');
            var map = new google.maps.Map(mapElement, mapOptions);
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(32.891474, 130.995287),
                icon: "/english/common/images/tokai-designator.png",
                map: map,
                title: ''
            });
        }
    });
})(jQuery);
