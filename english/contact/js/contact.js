(function($){
	$(function() {
		$('.csBox select').customSelect();
		$('.csBox select').on('change', function() {
			var $target = $(this).next().find('.customSelectInner');
			var selectedText = $(this).find('option:selected').text();
			if (selectedText == "Select" || selectedText == "Select country") {
				$target.css('color', '#afafaf');
			} else {
				$target.css('color', '#222');
			}
		});
	});
})(jQuery);
